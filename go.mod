module codeberg.org/mchack/svenska-jargongfilen

go 1.23.4

require (
	github.com/yuin/goldmark v1.7.8
	go.abhg.dev/goldmark/anchor v0.1.1
	golang.org/x/text v0.21.0
)
