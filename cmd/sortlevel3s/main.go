package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"golang.org/x/text/collate"
	"golang.org/x/text/language"
)

func main() {
	var outFileName string
	var overwrite bool
	flag.StringVar(&outFileName, "o", "", "set output file")
	flag.BoolVar(&overwrite, "f", false, "overwrite existing files")
	flag.Parse()

	if outFileName == "" {
		fmt.Fprintf(os.Stderr, "Set an output file using -o\n")
		os.Exit(1)
	}

	if len(flag.Args()) != 1 {
		fmt.Fprintf(os.Stderr, "Give me a markdown file and I'll sort the level 3 headings\n")
		os.Exit(1)
	}

	md, err := os.ReadFile(flag.Args()[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	var out strings.Builder
	scanner := bufio.NewScanner(bytes.NewReader(md))
	level3s := []string{}
	currentLevel3 := ""

	for scanner.Scan() {
		lineLF := scanner.Text() + "\n"
		switch {
		case strings.HasPrefix(lineLF, "### "):
			if currentLevel3 != "" {
				level3s = append(level3s, currentLevel3)
			}
			currentLevel3 = lineLF
		case strings.HasPrefix(lineLF, "# ") || strings.HasPrefix(lineLF, "## "):
			if currentLevel3 != "" {
				level3s = append(level3s, currentLevel3)
				sortSlice(level3s)
				out.WriteString(strings.Join(level3s, ""))
				currentLevel3 = ""
				level3s = []string{}
			}
			out.WriteString(lineLF)
		default:
			if currentLevel3 != "" {
				currentLevel3 += lineLF
			} else {
				out.WriteString(lineLF)
			}
		}
	}

	if err = scanner.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "Scan failed: %s\n", err)
		os.Exit(1)
	}

	if currentLevel3 != "" {
		level3s = append(level3s, currentLevel3)
		sortSlice(level3s)
		out.WriteString(strings.Join(level3s, ""))
	}

	flag := os.O_WRONLY | os.O_CREATE
	if !overwrite {
		flag |= os.O_EXCL
	}
	f, err := os.OpenFile(outFileName, flag, 0o600)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
	defer f.Close()

	if _, err = f.WriteString(out.String()); err != nil {
		fmt.Fprintf(os.Stderr, "WriteString failed %s\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "wrote %s\n", outFileName)
}

func sortSlice(slice []string) {
	trimRE := regexp.MustCompile(`^#* `)
	collator := collate.New(language.Swedish)

	sort.Slice(slice, func(i, j int) bool {
		iStr := trimRE.ReplaceAllString(strings.SplitN(slice[i], "\n", 2)[0], "")
		jStr := trimRE.ReplaceAllString(strings.SplitN(slice[j], "\n", 2)[0], "")
		iFloat, iNumeric := parseFloat(iStr)
		jFloat, jNumeric := parseFloat(jStr)
		switch {
		case iNumeric && jNumeric && iFloat != jFloat:
			return iFloat < jFloat
		case iNumeric:
			return true
		case jNumeric:
			return false
		default:
			iStr = strings.TrimLeft(iStr, "-*")
			jStr = strings.TrimLeft(jStr, "-*")
			return collator.CompareString(iStr, jStr) < 0
		}
	})
}

func parseFloat(s string) (float64, bool) {
	f, err := strconv.ParseFloat(numPrefix(s), 64)
	return f, err == nil
}

func numPrefix(s string) string {
	// This only covers what we need
	re := regexp.MustCompile(`^([-+]?[0-9]+(\.[0-9]+)?)(-.*)?`)
	match := re.FindStringSubmatch(s)
	if len(match) > 1 {
		return match[1]
	}
	return ""
}
