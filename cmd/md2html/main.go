package main

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"os"
	"strings"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
	"go.abhg.dev/goldmark/anchor"
)

func main() {
	var overwrite bool
	flag.BoolVar(&overwrite, "f", false, "overwrite existing files")
	flag.Parse()

	if len(flag.Args()) != 1 {
		fmt.Fprintf(os.Stderr, "Give me exactly one .md file")
		os.Exit(1)
	}

	mdFileName := flag.Args()[0]

	md, err := os.ReadFile(mdFileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	html, err := convert(md)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	baseName := strings.TrimSuffix(mdFileName, ".md")
	if err = writeFile(baseName+".html", html, overwrite); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func convert(md []byte) (string, error) {
	gm := goldmark.New(
		goldmark.WithExtensions(
			extension.Strikethrough,
			&anchor.Extender{
				Texter:   anchor.Text("#"),
				Position: anchor.Before,
				Attributer: anchor.Attributes{
					"class": "fragmentlink",
				},
			},
		),
		goldmark.WithParserOptions(
			parser.WithASTTransformers(
				util.Prioritized(linkTransformer{}, 0),
			),
			parser.WithAutoHeadingID(),
		),
	)

	ctx := parser.NewContext(parser.WithIDs(newIDs()))

	var html bytes.Buffer
	if err := gm.Convert(md, &html, parser.WithContext(ctx)); err != nil {
		return "", fmt.Errorf("Convert failed: %w", err)
	}

	return html.String(), nil
}

func writeFile(fileName string, html string, overwrite bool) error {
	data := make(map[string]any)
	data["html"] = template.HTML(html)

	templ, err := template.ParseFiles("template.html")
	if err != nil {
		return fmt.Errorf("template ParseFiles failed: %w", err)
	}

	var out bytes.Buffer
	if err = templ.Execute(&out, data); err != nil {
		return fmt.Errorf("template Execute failed: %w", err)
	}

	flag := os.O_WRONLY | os.O_CREATE
	if !overwrite {
		flag |= os.O_EXCL
	}
	f, err := os.OpenFile(fileName, flag, 0o600)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err = f.Write(out.Bytes()); err != nil {
		return fmt.Errorf("Write failed: %w", err)
	}

	fmt.Fprintf(os.Stdout, "wrote %s\n", fileName)

	return nil
}

type linkTransformer struct{}

func (t linkTransformer) Transform(doc *ast.Document, _ text.Reader, _ parser.Context) {
	err := ast.Walk(doc, func(n ast.Node, entering bool) (ast.WalkStatus, error) {
		if entering {
			if n.Kind() == ast.KindLink {
				l, ok := n.(*ast.Link)
				if !ok {
					fmt.Fprintf(os.Stderr, "KindLink is not Link?!\n")
					os.Exit(1)
				}
				// fmt.Fprintf(os.Stderr, "%s\n", l.Destination)
				if l != nil && strings.HasPrefix(string(l.Destination), "http") {
					n.SetAttributeString("target", []byte("_blank"))
					n.SetAttributeString("rel", []byte("noopener noreferrer"))
				}
			}
		}
		return ast.WalkContinue, nil
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "ast.Walk failed: %s\n", err)
		os.Exit(1)
	}
}

type ids struct {
	values map[string]bool
}

func newIDs() parser.IDs {
	return &ids{
		values: map[string]bool{},
	}
}

// Generate generates generous HTML5 IDs, according to:
// https://html.spec.whatwg.org/multipage/dom.html#the-id-attribute
// "must contain at least one character. The value must not contain
// any ASCII whitespace."
func (s *ids) Generate(value []byte, kind ast.NodeKind) []byte {
	value = util.TrimLeftSpace(value)
	value = util.TrimRightSpace(value)

	result := strings.Map(func(r rune) rune {
		switch r {
		case 0x9, 0xa, 0xd, 0x20: // HT, LF, CR, SPC
			return '-'
		default:
			return r
		}
	}, util.BytesToReadOnlyString(value))

	if len(result) == 0 {
		if kind == ast.KindHeading {
			result = "heading"
		} else {
			result = "id"
		}
	}

	if _, ok := s.values[result]; !ok {
		s.values[result] = true
		return []byte(result)
	}
	for i := 1; ; i++ {
		newResult := fmt.Sprintf("%s-%d", result, i)
		if _, ok := s.values[newResult]; !ok {
			s.values[newResult] = true
			return []byte(newResult)
		}
	}
}

func (s *ids) Put(value []byte) {
	s.values[util.BytesToReadOnlyString(value)] = true
}
