TARGETS = jargonswe.html

all: $(TARGETS)

.PHONY: md2html
md2html:
	go build ./cmd/$@
.PHONY: sortlevel3s
sortlevel3s:
	go build ./cmd/$@

jargonswe.html: md2html template.html
	./md2html -f jargonswe.md

check: all
	./check-word-links

sort: sortlevel3s
	./sortlevel3s -f -o sorted jargonswe.md
	mv -f sorted jargonswe.md

clean:
	rm -f $(TARGETS) md2html sortlevel3s

# This target is also used in .woodpecker/deploy.yml
.PHONY: dist
dist: all
	rm -rf dist
	mkdir dist
	printf "jargongfilen.se\n" >dist/.domains
	cp -af archive dist/
	cp -af jargonswe.html dist/index.html

deploy: dist
	rm -rf pages
	git clone -b pages --single-branch --depth=1 git@codeberg.org:mchack/svenska-jargongfilen.git pages
	cp -af dist/. pages/
	git -C pages add --all
	@git -C pages diff --cached --quiet \
          && printf "* No changes to deploy\n" \
          || { git -C pages commit -m "Bygg"; \
               printf "* You may look a the new pages commit: git -C pages show\n"; \
               printf "* And run the last step: git -C pages push\n"; }
