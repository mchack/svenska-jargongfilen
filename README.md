# Svenska jargongfilen

Ett lexikon över svensk hackerslang och uppslagsbok för svensk
hackerdom. Se:

https://jargongfilen.se/

för den renderade versionen. Denna byggs och publiceras från main vid
midnatt varje dag med hjälp av Codeberg's Woodpecker CI-instans:
https://ci.codeberg.org/repos/14077

Markdown-källan är i [jargonswe.md](jargonswe.md).

För att bygga lokalt, kör `make`. Kräver `go` för att bygga `md2html`
(baserad på Goldmark som stöder CommonMark). Mallen
[template.html](template.html) används vid renderingen.

## Äldre versioner

Under [archive](archive) finns äldre versioner av jargongfilen i lite
olika format. I kronologisk ordning:

- [Hacker-uttryck på KTH (NADA och Elektro), LiTH samt
  Uppsala](archive/hackerswe.txt) utan år men troligen 80-tal. Troligen
  först publicerad på papper i [Stackens](https://stacken.kth.se/)
  tidning [StackPointer 2, 1981 sidan
  49](archive/stackpointer-1981-2.pdf),
  [källa](http://elvira.stacken.kth.se/~mz/sp/stackpointer-1981-2.pdf).
  
- [Svenska Hackademins Ordlista v4.0c](archive/shol4.0c.html) från 1996.

- [Svenska jargongfilen 2003-05-05
  (13)](archive/texinfo/hackerswe.html). Källkod i TeXinfo och
  Info-versioner under [texinfo](archive/texinfo). Underhölls från
  90-talets mitt till 2003.

## Redigering

- Bevara humorn! Skriv nya ord med liknande humor. Är definitionen enkel
  och kort så kanske du kan införa lite humor i exemplen.
- Markera eventuellt uttal med \/uttal\/.
- Markera eventuellt organisationsursprung före förklaring, till
  exempel: [Lysator]. Om det behövs mer etymologisk utbredning så gör
  det i själva definitionstexten istället.
- Skriv ut förkortningar om det inte är etablerade namn på saker. "IBM"
  behöver till exempel inte skrivas ut. Hellre "till exempel" än "t ex".
- Håll raderna i Markdown-filen under 72 tecken. Slå på
  [EditorConfig](https://editorconfig.org/).
- Små bokstäver på uppslagsorden om det inte är egennamn eller
  förkortningar.
- Håll gärna uppslagsorden sorterade -- rubriker på nivår 3, under sina
  respektive nivå 2-rubriker. Det finns ett verktyg som kan hjälpa till,
  se `make sort`. Detta sorterar filen `jargonswe.md` på plats
  (in-place).
